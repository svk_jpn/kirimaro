# 標準パッケージ
import os
# 外部パッケージ
import cv2
import numpy as np

def crop_roi(img, roi):
	''' ROIを切り取った部分画像を取得
	roi : 入力画像に対するROI(x, y, w, h)
	'''
	start_x = roi[0]
	start_y = roi[1]
	end_x = roi[0] + roi[2]
	end_y = roi[1] + roi[3]
	return img[start_y:end_y, start_x:end_x]

def imread(filename, flags=cv2.IMREAD_COLOR, dtype=np.uint8):
    try:
        n = np.fromfile(filename, dtype)
        img = cv2.imdecode(n, flags)
        return img
    except Exception as e:
        print(e)
        return None

def imwrite(filename, img, params=None):
    try:
        ext = os.path.splitext(filename)[1]
        result, n = cv2.imencode(ext, img, params)
        if result:
            with open(filename, mode='w+b') as f:
                n.tofile(f)
            return True
        else:
            return False
    except Exception as e:
        print(e)
        return False

def match_bin_map(test, tgt, fg_weight = 0.5):
	''' Ture/Falseの2値マップのマッチング
	test : 評価対象のマップ
	tgt : 評価基準のマップ
	fg_ration : tgtのTrueの箇所での正解率のウェイト
	'''
	fg_area = np.sum(tgt == True)
	fg_ok = np.sum(test[tgt == True]) / fg_area # 前景部分の正解率
	bg_area = np.sum(tgt == False)
	bg_ok = 1.0 - (np.sum(test[tgt == False]) / bg_area) # 背景部分の正解率
	return fg_ok * fg_weight + bg_ok * (1.0 - fg_weight)

def bynarize_bgr(img, color=[ 255, 255, 255 ], th_sq = 5):
	'''指定色に近いかどうかでの2値化
	params:
		img : 2値化対象のBGRマップ
		color : 基準色(BGR)
		th_sq : 基準色からのL2距離に対する2値化閾値
	'''
	color = np.array(color)
	return np.sum((img - color) ** 2, axis=2) <= th_sq

def read_frames(cap, fid, num = 1):
	'''複数フレームの読みとばし
	cap.set(cv2.CAP_PROP_POS_FRAMES, x)は毎回先頭からサーチするっぽいので
	漸次的にフレームを進めるならcap.read()を繰り返し呼ぶ方が速い
	params:
		cap : VideoCaptureインスタンス
		num : 読み込みフレーム数
	'''
	if num < 1:
		num = 1
	for _ in range(num):
		ret, frame = cap.read()
		fid += 1
		if ret == False:
			break
	return fid, ret, frame
