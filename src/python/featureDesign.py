'''featureDesign.py
ゲーム画面を解析するにあたり
特徴量等をグラフに描いて挙動を確認するのに使っていたスクリプト。
実験用に適宜変更して使用ください。
'''
# 標準パッケージ
import os
from collections import deque
# 外部パッケージ
from absl import app, flags
import cv2
import numpy as np
# ユーザーパッケージ
from tools.graphImage import ImageGraph
from tools.avg2Analyzer import AVG2Analyzer

FLAGS = flags.FLAGS
flags.DEFINE_string('src_mov', 'kiri_test/2021-12-29 15-55-21.mp4', '入力')
flags.DEFINE_string('dst_mov', 'test_resource/dst/kiri_result.mp4', '出力')

# 固定パラメータ
inner_size = [ 960, 540 ] # 内部で処理時の画像サイズ
graph_hight = 100

# カラー辞書
colors = {
	'black':(0,0,0),
	'red':(0,0,255),
	'green':(0,255,0),
	'blue':(255,0,0),
	'purple':(255,0,255),
}

def main(args):
	# 動画読み込み
	cap = cv2.VideoCapture(FLAGS.src_mov)
	fps = cap.get(cv2.CAP_PROP_FPS)
	# 出力動画の初期化
	dst_size = (inner_size[0], inner_size[1] + graph_hight)
	fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
	os.makedirs(os.path.dirname(FLAGS.dst_mov), exist_ok=True)
	writer = cv2.VideoWriter(FLAGS.dst_mov, fourcc, fps, dst_size)	
	g = ImageGraph((inner_size[0], graph_hight))
	# 分析器の初期化
	analyzer = AVG2Analyzer()
	que_round1 = deque([0.0] * inner_size[0]) # ROUND1画面スコア
	que_vs = deque([0.0] * inner_size[0]) # VS画面スコア
	# 解析処理
	ret, frame = cap.read() # 最初のフレームを読み込む
	cnt = 1
	while(ret):
		# 前処理
		frame = cv2.resize(frame, inner_size) # 内部処理サイズにリサイズ
		frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # グレースケール化
		# 分析
		g.clearCanvas() # このフレームの描画をクリア
		# ROUND1表示完了
		val = analyzer.check_round1(frame)
		que_round1.append(val)
		que_round1.popleft()
		g.plotValues(que_round1, colors['blue'])
		# VS表示完了
		#val = analyzer.check_match_vs(frame)
		#que_vs.append(val)
		#que_vs.popleft()
		#g.plotValues(que_vs, colors['red'])
		frame_graph = g.getGanvas() # グラフ画像を取得
		frame = np.concatenate([frame, frame_graph], axis=0)
		writer.write(frame)
		ret, frame = cap.read() # 次のフレームを読み込む

		cnt += 1

	writer.release()
	cap.release()

if __name__ == '__main__':
	app.run(main)
