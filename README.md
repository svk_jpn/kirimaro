# 斬り麿 -Kirimaro-
----


# 1. 概要
だらーっと撮った格ゲーの動画を試合単位でぶった切りたい。  
ぶった切って試合のとこだけ抜いて、振り返りとか動画編集を簡単にしたい。  
そんな怠惰な欲望を叶えるために、人類は立ち上がった。  

# 2. 機能
(**現在開発中!**)  
実装予定機能は以下。  

- 試合の開始位置を検出して適切に動画カット  
- 使用キャラクターの抽出  
- プレイヤー情報の抽出  

対応はタイトル別にする予定。  
パフォーマンス改善などは追々で。

# 3. Install

## 3.1. 確認環境
Windows10で動作を確認しています。Pythonとffmpegが必要です。  
今のところGPUは使ってないけど…Deep Learningするようになったら使うかもしれない。  
Pythonとffmpegが動けばOSは関係ないとは思います。  

## 3.2. 外部ソフトウェア
切り出しには[ffmpeg](https://ffmpeg.org/download.html)を使用しているので、事前にffmpegをインストールし、パスを通してください。  
現状ffmpegを叩いているだけなので、他のソフトに対応させたい！というやる気マンは是非forkしたりpull request送ったりしよう!  

## 3.2. Python版
プロトタイピング目的で作成しています。  

### 3.2.1. PyInstaller版
Pythonをインストールしなくても良いバージョンは[こちら](https://drive.google.com/drive/folders/1sO2f4ZCFXMbVmxoGHmvucGu1Coi2LDus?usp=sharing)で配布しています。  
`kirimaro.conf`を用途に合わせて編集して、`kirimaro.bat`をダブルクリックで動く形にはしています。  
このケースでもffmpegのインストールとffmpeg.exeのあるディレクトリへのパスを環境変数に追加する必要はあります。  

### 3.2.2. 環境
Pythonの環境は各自で作成してください(突き放し)。  
開発はPython3.9.7で行っています。多分3.7ぐらいでも動くと思います。  
packageはrequirements.txtを参照してください。  
環境を壊したくない人は`venv`, `pipenv`, `conda`などの環境下で使用してください。  
```
pip install -r requirements.txt
```

# 4.動作

## 4.1. 斬り麿の動作
斬り麿は指定した入力ディレクトリ(`--src_dir`)直下にある拡張子`.mp4`のファイルを解析して、その動画中の対戦1ゲーム毎に動画を分割して指定した出力ディレクトリ(`--dst_dir`)に出力します。  

入力ディレクトリが`src`, 出力ディレクトリが`dst`の例を以下に挙げます。  
```
src -+- movie1.mp4
     |
     +- movie2.mp4
```
この場合出力ディレクトリには以下の様に対戦動画が出力されます。 

```
dst-+- movie1 -+--- movie1_00_00_03.png (対戦前のマッチ画面の画像。再生時間00:00:03)
    |          |
    |          +--- movie1_00_01_24.mp4 (元動画の00:01:24から始まる対戦動画)
    |          |
    |          +--- movie1_01_40_00.mp4 (元動画の01:40:00から始まる対戦動画)
    | 
    +- movie2 -+- (以下同文) 
```
対戦マッチ画面のスクショ(ある場合のみ)と対戦動画が出力されます。

## 4.2. Python版の実行
リポジトリのルートディレクトリから`kirimaro.py`を実行してください。  
以下は`src`以下の`mp4`ファイルを`dst`に分割出力する例。  
```
python src/python/kirimaro.py --src_dir src --dst_dir dst
```
オプションについては以下に説明します。  
**太字**のオプションは設定してください。

|オプション名|説明|
|:-|:-|
|**src_dir**|対象の動画の入っているディレクトリを指定します。このディレクトリ直下の`*.mp4`のファイルが動画切り出し対象になります。|
|**dst_dir**|切り取った動画が出力されるディレクトリ。|
|analyzer|ゲーム画面の解析エンジン名を指定します。|
|list|動画の切り取りリスト(中間生成ファイル)の保存パス。これも弄らなくていいです。|
|resume|作成済みの動画切り取りリストから動画を作成する。切り取りリストを手動で調整して出し直したい方向け。|
|skip_analyze|何フレーム置きに解析するか。大きくすると切り取りのフレーム解像度が下がるが高速に。あまり大きいと失敗するので10以下推奨。|
|reencoding|Falseにした場合は開始位置が直近のIフレームまで遡り、無劣化で切り出す(再エンコードなし)。|

オプションはテキストファイルに書いて読み込ませることもできます。`kirimaro.conf`を読み込む例を挙げます。  
```
python src/python/kirimaro.py --flagfile kirimaro.conf
```
# 5. 対応予定など

## 5.1. 対応タイトル

|analyzer|タイトル|備考|
|:-|:-|:-|
|GGST|[Guilty Gear Strive](https://www.guiltygear.com/ggst/jp/)|初期バージョン成完。観戦試合には非対応。|
|CYB|[CYBERBOTS -FULLMETAL MADNESS-](https://store.steampowered.com/app/1515950/Capcom_Arcade_Stadium/?l=japanese)|Capcom Arcade Stadium版の通常表示のみ対応。<br>攻略Wikiは[ここ](https://gitlab.com/svk_jpn/cyberbots_lab/-/wikis/home)|
|AVG2|[Advanced Variable Geo 2](https://www.jp.playstation.com/software/title/jp0741npjj00761_000000000000000001.html)|PSアーカイブ版の全画面キャプチャの動画のみ対応。<br>オリジナル解像度対応はまぁ、データが集まったらで。<br>攻略Wikiは[ここ](https://w.atwiki.jp/avg2/)|

## 5.2. 対応予定タイトル
基本的にはやりたいやつか、協力者が多いタイトルをやっていきます。  
需要の有無とか関係ねー!!!(仕事じゃないので自由!)  
例外としてパターン上めちゃくちゃ難しい場合は別途考える。  

しばらくはRust版のAnalyzerの作成を優先予定。  
(次期対応タイトル検討中)

# 6. 開発方針
基本的にPythonでプロトタイプ作って、ある程度固まったらお勉強がてらRust版も作る。  
DeepLearningを使用する場合は学習はPythonのままだと思う。  
