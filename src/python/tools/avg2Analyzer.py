'''Advance Variable Geo 2ゲーム画面の解析
'''
# 標準パッケージ
# 外部パッケージ
import numpy as np
import cv2
# ユーザーパッケージ
import tools.imgutil as iu

class AVG2Analyzer:
	'''ゲーム開始画面の分析モジュール
	ゲーム開始画面で1Pのキャラクター名がフェードインしきった箇所の解析用
	'''
	analyze_size = [960, 540] # 分析時標準サイズ
	# 各種ROI
	roi_round1 = [625,218,64,112] # ROUND1表示のROI
	roi_vs = [412,192,128,128] # VS表示のROI

	# 色に関する閾値
	bgr_rlogo= [5,10,245]
	cth_rlogo= 250
	bgr_bllogo= [0,0,4]
	cth_bllogo= 10
	# 特徴量に対する閾値
	fth_vs = 0.90
	fth_round1 = 0.83

	# 動画切り取りオフセット
	offset_round1 = -72
	offset_vs = -340

	def __init__(self, skip_analyze = 6):
		'''
		params:
			skip_analyze : 何フレームおきに解析を行うか。
		'''
		self.skip_analyze = skip_analyze # 何フレームおきに解析するか
		# 'ROUOND1'表示のテンプレート準備
		img = iu.imread('params/AVG2/ROUND_1.bmp', cv2.IMREAD_COLOR)
		self.tmpl_round1 = iu.bynarize_bgr(img , self.bgr_rlogo, self.cth_rlogo)
		# 'VS'表示のテンプレート準備
		img = iu.imread('params/AVG2/VS.bmp', cv2.IMREAD_COLOR)
		self.tmpl_vs = iu.bynarize_bgr(img , self.bgr_bllogo, self.cth_bllogo)
		# キーフレーム探索タスクリスト
		self.search_tasks = [
			self.search_round1,
			self.search_match_vs,
		]

	'''
	各種フレームの判定用メソッド
	'''
	def check_round1(self, img:np.ndarray):
		'''ROUND1の表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_round1)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_rlogo, self.cth_rlogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_round1)
		return  score

	def check_match_vs(self, img:np.ndarray):
		'''キャラセレ画面のVS表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_vs)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_bllogo, self.cth_bllogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_vs)
		return  score

	'''
	キーフレーム探索タスク
	'''
	def search_round1(self, fid:int):
		'''ROUND1表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_round1(self.frame) > self.fth_round1:
			if self.f_round1 == False:
				self.isFindFootage = False
				self.fid_start = fid
				self.fid_end = None
				self.f_round1 = True
				skip_frames = 400
		else:
			self.f_round1 = False
		return skip_frames

	def search_match_vs(self, fid:int):
		'''VS画面表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_match_vs(self.frame) > self.fth_vs:
			if self.f_vs == False:
				if self.fid_start is not None:
					self.isFindFootage = True # 切り出し区間検出
					self.fid_end = fid
				skip_frames = 180
			self.f_vs = True
		else:
			self.f_vs = False
		return skip_frames

	'''解析シーケンス
	'''
	def analyze(self, cap, output_tail = False):
		'''動画の解析
		params : 
			cap : VideoCaptureインスタンス
			output_tail : 動画の最後にResult画面の無い対戦動画があった場合出力するか
		return(yield) :
			[fid_info, fid_start, fid_end]
			fid_info : マッチ情報画面のフレーム番号
			fid_start : 切り出し開始のフレーム番号
			fid_end : 切り出し終了のフレーム番号+1
		'''
		# ゲーム固有の解析フラグの初期化
		self.f_round1 = False # ROUND1が表示されているか
		self.f_vs = False # VS画面が表示されているか
		self.fid_info = -1	# マッチ情報取得の動画フレーム
		self.fid_start = None	# バトル開始のフレーム番号

		# 共通の初期化処理
		cap.set(cv2.CAP_PROP_POS_FRAMES, 0) # 読み出しフレーム位置リセット
		fps = cap.get(cv2.CAP_PROP_FPS) # 動画のFPS
		fid = 0
		ret, self.frame = cap.read() # 最初のフレームを読み込む
		self.isFindFootage = False #　動画区間を発見したか

		# 解析処理
		while(ret):
			# 前処理
			self.frame = cv2.resize(self.frame, self.analyze_size)
			# キーフレーム探索
			for task in self.search_tasks:
				skip_frames =  task(fid)
				if skip_frames > 0:
					break # キーフレーム検出
			else:
				skip_frames = self.skip_analyze # キーフレームが見つからなかった
			# 切り出し区間情報伝達
			if self.isFindFootage: # 切り出し区間の発見
				# フレーム情報の出力
				yield { 
					'fps' : fps,
					'fid_info' : -1, 
					'fid_start' : self.fid_start + self.offset_round1, 
					'fid_end' : self.fid_end + self.offset_vs + 1 }
				# 区間情報のリセット
				self.isFindFootage = False
				self.fid_start = None
				self.fid_end = None
			# 次のフレームへ
			fid, ret, self.frame = iu.read_frames(cap, fid, skip_frames)

		# 最後の動画が尻切れだった場合に出力する
		if output_tail and self.fid_start is not None:
			yield { 
				'fps' : fps,
				'fid_info' : -1, 
				'fid_start' : self.fid_start + self.offset_round1, 
				'fid_end' : self.fid_end + self.offset_vs + 1 }