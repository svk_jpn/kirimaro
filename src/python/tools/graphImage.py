'''matplotlibのグラフを画像出力する
'''
# 標準パッケージ
# 外部パッケージ
import numpy as np
import cv2
# ユーザーパッケージ

class ImageGraph:
	def __init__(self, size, bg_color=(255, 255, 255)):
		'''
		size : (w, h)のサイズ指定
		'''
		self.shape = [size[1], size[0], 3] # 出力のnumpyのshapeを保存
		self.bg_color = np.array(bg_color, dtype=np.uint8)
		self.clearCanvas()

	def clearCanvas(self, bg_color=None):
		'''キャンバスのクリア
		'''
		if bg_color:
			self.canvas = np.ones(self.shape, dtype=np.uint8) * bg_color
		else:
			self.canvas = np.ones(self.shape, dtype=np.uint8) * self.bg_color

	def plotValues(self, values, plot_color=(255, 0, 0)):
		'''キャンバスのクリア
		'''
		vals = list(values)[-self.shape[1]:]
		hight = self.shape[0] - 1
		for x in range(len(vals) - 1):
			pt_start = (x, int((1 - vals[x]) * hight))
			pt_end = (x + 1, int((1 - vals[x + 1]) * hight))
			cv2.line(self.canvas, pt_start, pt_end, plot_color)



	def getGanvas(self):
		return self.canvas