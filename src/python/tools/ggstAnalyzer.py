'''ゲーム開始画面の解析
'''
# 標準パッケージ
# 外部パッケージ
import numpy as np
import cv2
# ユーザーパッケージ
import tools.imgutil as iu

class GGSTAnalyzer:
	'''ゲーム開始画面の分析モジュール
	ゲーム開始画面で1Pのキャラクター名がフェードインしきった箇所の解析用
	'''
	analyze_size = [1280, 720] # 分析時標準サイズ
	# 各種ROI
	pos_pname = [ # キャラクター名領域左上隅座標
		[ 168, 286 ],
		[ 168, 412 ]
	]
	roi_p1_tag = [168, 254, 88, 20]# Player 1 のタグROI
	roi_p1_3rd_badge = [1011, 53, 42, 40]# Player 1 の第3バッジのROI
	area_char_name = [ 456, 36 ] # キャラクター名領域のサイズ
	area_chek_start1 = [ 88, 36 ] # キャラクター名フェードインチェック領域のサイズ
	margin_v_start = 126 # ゲーム開始時の上下の黒帯マージン
	margin_v_normal = 72 # 通常の上下の黒帯マージン
	roi_duel = [468, 132, 344, 186] # ROUND開始時の'DUEL'の文字のROI
	roi_round1 = [590, 296, 100, 180] # ROUND1開始時の'1'の文字のROI
	roi_result = [512,655,255,25] # ROI

	# 輝度に対する閾値
	lth_start_font = 128 # ゲーム開始画面フォント2値化閾値
	lth_badge_in = 70 # バッジ表示完了閾値
	lth_duel = 248 # 'DUEL'文字輝度閾値
	lth_result = 63 # 'DUEL'文字輝度閾値
	# 色に関する閾値
	bgr_round1 = [15, 7, 186]
	cth_round1 = 58
	# 特徴量に対する閾値
	fth_p1tag_on = 0.70 # Player1タグの表示判定閾値
	fth_cname_on = 0.55 # キャラクター名表示完了判定閾値
	fth_duel_on = 0.75 # DUELの表示
	fth_r1_on = 0.75 # Round1の表示
	fth_result_on = 0.75 # Resultの表示

	# 動画切り取りオフセット
	offset_round1 = -50
	offset_result = -100

	def __init__(self, skip_analyze = 6):
		'''
		roi : 入力画像に対するROI(x, y, w, h)
		'''
		self.skip_analyze = skip_analyze # 何フレームおきに解析するか
		# 'Player1'タグのテンプレート準備
		img = iu.imread('params/GGST/tag_p1.bmp', cv2.IMREAD_GRAYSCALE)
		self.tmpl_p1tag = img < self.lth_start_font
		# 'DUEL'のテンプレート準備
		img = iu.imread('params/GGST/DUEL.bmp', cv2.IMREAD_GRAYSCALE)
		self.tmpl_DUEL = img > self.lth_start_font
		# 'round 1'のテンプレート準備
		img = iu.imread('params/GGST/round1.bmp', cv2.IMREAD_COLOR)
		self.tmpl_round1 = iu.bynarize_bgr(img , self.bgr_round1, self.cth_round1)
		# Result画面'GUILTY GEAR STRIVE'のテンプレート準備
		img = iu.imread('params/GGST/result_btm.bmp', cv2.IMREAD_GRAYSCALE)
		self.tmpl_result_btm = img > self.lth_result
		# キーフレーム探索タスクリスト
		self.search_tasks = [
			self.search_cname_frame,
			self.search_pinfo_frame,
			self.search_d1start_frame,
			self.search_result_frame,
		]

	'''
	各種フレームの判定用メソッド
	'''
	def check_match_cname(self, img_gray:np.ndarray):
		'''キャラクター名表示が完了しているか
		'''
		img_roi = iu.crop_roi(img_gray, self.pos_pname[0] + self.area_chek_start1)
		area = self.area_chek_start1[0] * self.area_chek_start1[1]
		score = np.sum(img_roi < self.lth_start_font) / area
		return  score

	def check_match_p1tag(self, img_gray:np.ndarray):
		''' Player 1 タグが表示されているか
		'''
		tgt = iu.crop_roi(img_gray, self.roi_p1_tag)
		score = iu.match_bin_map(tgt < self.lth_start_font, self.tmpl_p1tag)
		return  score

	def check_match_pinfo(self, img:np.ndarray):
		'''Player情報が表示されているか
		'''
		tgt = iu.crop_roi(img, self.roi_p1_3rd_badge)
		return np.mean(tgt)

	def check_duel1(self, img_bgr:np.ndarray, img_gray:np.ndarray):
		''' 'DUEL 1'が表示されているか
		'''
		duel_val = iu.match_bin_map(iu.crop_roi(img_gray, self.roi_duel) > self.lth_duel, self.tmpl_DUEL)
		crop_bgr = iu.crop_roi(img_bgr, self.roi_round1)
		crop_bin = iu.bynarize_bgr(crop_bgr , self.bgr_round1, self.cth_round1)
		round1_val = iu.match_bin_map(crop_bin , self.tmpl_round1)
		return duel_val > self.fth_duel_on and round1_val > self.fth_r1_on

	def check_result(self, img:np.ndarray):
		''' Result画面下の'GUILTY GEAR STRIVE'が表示されているか
		'''
		# 'GUILTY GEAR STRIVE'の文字のテンプレートマッチ
		tgt = iu.crop_roi(img, self.roi_result)
		score = iu.match_bin_map(tgt > self.lth_result, self.tmpl_result_btm)
		return  score

	'''
	キーフレーム探索タスク
	'''
	def search_cname_frame(self, fid):
		'''キャラクター情報表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_match_p1tag(self.frame_gray) > self.fth_p1tag_on:
			if self.check_match_cname(self.frame_gray) > self.fth_cname_on:
				if self.f_cinfo == False:
					self.fid_info = fid
					self.f_cinfo = True
					skip_frames = 50
		else:
			self.f_cinfo = False
		return skip_frames

	def search_pinfo_frame(self, fid):
		'''プレイヤー情報表示のフレーム探索
		'''
		skip_frames = 0
		if self.f_cinfo:
			if self.f_pinfo == False:
				if self.check_match_pinfo(self.frame_gray) > self.lth_badge_in:
					self.fid_info = fid
					self.f_pinfo = True
					skip_frames = 300
		else:
			self.f_pinfo = False
		return skip_frames

	def search_d1start_frame(self, fid):
		'''DUEL1開始画面のフレーム探索
		'''
		skip_frames = 0
		if self.check_duel1(self.frame, self.frame_gray):
			if self.f_bstart == False:
				self.fid_start = fid
				self.f_bstart = True
				skip_frames = 300
		else:
			self.f_bstart = False
		return skip_frames

	def search_result_frame(self, fid):
		'''Result画面のフレーム探索
		'''
		skip_frames = 0
		if self.check_result(self.frame_gray) > self.fth_result_on:
			if  self.f_result == False:
				if self.fid_start is not None:
					self.isFindFootage = True # 切り出し区間検出
				else:
					# 区間情報のリセット
					self.fid_info = -1
					self.fid_start = None
				skip_frames = 300
			self.f_result = True
		else:
			self.f_result = False
		return skip_frames

	'''解析シーケンス
	'''
	def analyze(self, cap, output_tail = False):
		'''動画の解析
		params : 
			cap : VideoCaptureインスタンス
			output_tail : 動画の最後にResult画面の無い対戦動画があった場合出力するか
		return(yield) :
			[fid_info, fid_start, fid_end]
			fid_info : マッチ情報画面のフレーム番号
			fid_start : 切り出し開始のフレーム番号
			fid_end : 切り出し終了のフレーム番号+1
		'''
		# ゲーム固有の解析フラグの初期化
		self.f_cinfo = False # キャラ情報が表示されているか
		self.f_pinfo = False # プレイヤー情報が表示されているか
		self.f_bstart = False # バトル開始画面か
		self.f_result = False # Reslt画面か
		self.fid_info = -1	# マッチ情報取得の動画フレーム
		self.fid_start = 0	# バトル開始のフレーム番号

		# 共通の初期化処理
		cap.set(cv2.CAP_PROP_POS_FRAMES, 0) # 読み出しフレーム位置リセット
		fps = cap.get(cv2.CAP_PROP_FPS) # 動画のFPS
		fid = 0
		ret, self.frame = cap.read() # 最初のフレームを読み込む
		self.isFindFootage = False #　動画区間を発見したか

		# 解析処理
		while(ret):
			# 前処理
			self.frame = cv2.resize(self.frame, self.analyze_size)
			self.frame_gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
			# キーフレーム探索
			for task in self.search_tasks:
				skip_frames =  task(fid)
				if skip_frames > 0:
					break # キーフレーム検出
			else:
				skip_frames = 0 # キーフレームが見つからなかった
			# 切り出し区間情報伝達
			if self.isFindFootage: # 切り出し区間の発見
				# フレーム情報の出力
				yield { 
					'fps' : fps,
					'fid_info' : self.fid_info, 
					'fid_start' : self.fid_start + self.offset_round1, 
					'fid_end' : fid+ self.offset_result + 1 }
				# 区間情報のリセット
				self.isFindFootage = False
				self.fid_info = -1
				self.fid_start = None
			# 次のフレームへ
			fid, ret, self.frame = iu.read_frames(cap, fid, self.skip_analyze)

		# 最後の動画が尻切れだった場合に出力する
		if output_tail and self.fid_start is not None:
			yield { 
				'fps' : fps,
				'fid_info' : self.fid_info, 
				'fid_start' : self.fid_start + self.offset_round1, 
				'fid_end' : fid }