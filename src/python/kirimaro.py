'''detectGameStart.py
ゲーム開始画面の検出
'''
# 標準パッケージ
import os
from glob import glob
import subprocess
# 外部パッケージ
from absl import app, flags
import cv2
import pandas as pd
import numpy as np
# ユーザーパッケージ
from tools.ggstAnalyzer import GGSTAnalyzer
from tools.cybAnalyzer import CYBAnalyzer
from tools.avg2Analyzer import AVG2Analyzer

FLAGS = flags.FLAGS
flags.DEFINE_string('src_dir', 'kiri_test', '入力')
flags.DEFINE_string('dst_dir', 'result', '出力')
flags.DEFINE_string('analyzer', 'AVG2', 'アナライザータイプ')
flags.DEFINE_string('list', 'footages.csv', '切り出しリスト')
flags.DEFINE_bool('resume', False, '切り出しリストからの再開か')
flags.DEFINE_integer('skip_analyze', 6, '何フレーム置きに解析するか')
flags.DEFINE_bool('reencoding', False, '再エンコーディング or 無劣化Iフレーム単位切り出し')

# 動画解析器
analyzers = {
	'GGST' : GGSTAnalyzer,	# Guilty Gear -Strive-
	'CYB' : CYBAnalyzer,	# Cyberbots -fullmetal maddness-
	'AVG2' : AVG2Analyzer,	# Advanced Variable Geo 2
}

def getTimestamp(frame, fps):
	'''タイムスタンプ文字列の作成
	'''
	sec = int(frame / fps)
	return f'{sec // 3600:02}_{(sec // 60) % 60:02}_{sec % 60:02}'	

def getKeyframeList(path):
	'''キーフレーム番号の一覧を取得
	'''
	cmd = f'ffprobe -v error -i "{path}" -select_streams v:0 -show_entries frame=pict_type -of flat'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	kf_list = []
	while True:
		line = proc.stdout.readline() # バッファから1行読み込む.
		if not line and proc.poll() is not None: # バッファが空 + プロセス終了.
			break
		# Iフレームのフレーム番号をリストに登録
		line = line.decode('utf-8')
		if '"I"' in line:
			kf_list.append(int(line.split('.')[2])) # フレーム番号を記録
	return np.array(kf_list)

def makeFootageList(src_dir, analyzer, dst_dir, reencoding=False, output_tail = False):
	'''動画解析をしながら完成次第ffmpegで別プロセスで切り出す
	'''
	paths = glob(f'{src_dir}/*.mp4')
	df = pd.DataFrame(columns=['src_path', 'fps', 'fid_info', 'fid_start', 'fid_end'])
	num = len(paths)
	for i, path in enumerate(paths):
		# 動画読み込み
		print(f'[{i+1}/{num}] : {os.path.basename(path)}')
		# キーフレームリスト作成
		print('Get Keyframe List...')
		kf_list = np.array([]) if reencoding == True else getKeyframeList(path)
		print('Start Analyze and Crop Footages...')
		cap = cv2.VideoCapture(path)
		p = None
		for params in analyzer.analyze(cap, output_tail):
			if p != None:
				p.wait()
				p = None
			p = sliceFootage(path, params, dst_dir, kf_list) # 動画切り出し
			params['src_path'] = path
			df = df.append(params, ignore_index=True)
		cap.release()
		if p != None:
			p.wait()
	return df

def sliceFootage(src_path, params, dst_root, kf_list):
	'''動画解析をしながら完成次第ffmpegで別プロセスで切り出す
	'''
	fps = params['fps']
	fid_info = params['fid_info']
	fid_start = params['fid_start']
	fid_end = params['fid_end']
	# 切り出し区間の計算
	if len(kf_list) > 0:
		fid_start = kf_list[np.sum(kf_list <= fid_start) - 1] # 直前のキーフレームにフィット
	start_sec = fid_start/fps
	duration_sec = fid_end/fps - start_sec
	# 出力準備(出力ファイル名等の整備)
	filename = os.path.basename(src_path)
	movname, ext = os.path.splitext(filename)
	dst_dir = os.path.join(dst_root, movname)
	os.makedirs(dst_dir, exist_ok=True)
	# 対戦者情報画像の保存
	if fid_info >= 0:
		ts = getTimestamp(fid_info, fps)
		img_path = f'{dst_dir}/{movname}_{ts}.png'
		cmd = f'ffmpeg -y -i "{src_path}" \
			-vf trim=start_frame={fid_info}:end_frame={fid_info + 1},setpts=PTS-STARTPTS \
			-an -vsync 0 "{img_path}"'
		print(cmd)
		subprocess.call(cmd)
	# 動画の切り出し
	ts = getTimestamp(fid_start, fps)
	mov_path = f'{dst_dir}/{movname}_{ts}{ext}'
	if len(kf_list) > 0:
		# キーフレーム単位で開始する場合の無劣化切り出し
		cmd = f'ffmpeg -y  -ss {start_sec:.3f} -i "{src_path}" -t {duration_sec:3f} -vcodec copy -acodec copy -async 1 "{mov_path}"'
	else:
		# 自由な位置から動画を作成できるが再エンコーディングを行う
		cmd = f'ffmpeg -y  -ss {start_sec:.3f} -i "{src_path}" -t {duration_sec:3f} "{mov_path}"'
	return subprocess.Popen(cmd)

def sliceFootages(flist, dst_root):
	'''切り出しリストに応じて動画を切り出し
	動画名と同じ名前のディレクトリを作り、その下にマッチ画面の画像と
	試合ごとに分割した動画を出力します。
	params:
		flist : 動画リストのdataframe
		dst_root : 出力先ルートディレクトリのパス
	return:
		なし
	'''
	num = len(flist)
	for i, rec in flist.iterrows():
		# 情報の取り出し
		src_path = rec['src_path']
		fps = rec['fps']
		fid_info = rec['fid_info']
		fid_start = rec['fid_start']
		fid_end = rec['fid_end']
		# 切り出し区間の計算
		start_sec = fid_start/fps
		duration_sec = fid_end/fps - start_sec
		# 出力準備(出力ファイル名等の整備)
		filename = os.path.basename(src_path)
		movname, ext = os.path.splitext(filename)
		dst_dir = os.path.join(dst_root, movname)
		os.makedirs(dst_dir, exist_ok=True)
		print(f'[{i+1}/{num}] : {filename}')
		# 対戦者情報画像の保存
		if fid_info >= 0:
			ts = getTimestamp(fid_info, fps)
			img_path = f'{dst_dir}/{movname}_{ts}.png'
			cmd = f'ffmpeg -y -i "{src_path}" \
				-vf trim=start_frame={fid_info}:end_frame={fid_info + 1},setpts=PTS-STARTPTS \
				-an -vsync 0 "{img_path}"'
			print(cmd)
			subprocess.call(cmd)
		# 動画の切り出し
		ts = getTimestamp(fid_start, fps)
		mov_path = f'{dst_dir}/{movname}_{ts}{ext}'
		cmd = f'ffmpeg -y -ss {start_sec:.3f} -i "{src_path}" -t {duration_sec:3f} "{mov_path}"'
		subprocess.call(cmd)

def main(args):
	os.makedirs(FLAGS.dst_dir, exist_ok=True)
	# リストの作成
	if FLAGS.resume == False:
		analyzer = analyzers[FLAGS.analyzer](FLAGS.skip_analyze)
		print(f'Start Analyze {FLAGS.analyzer} movies.')
		footage_list = makeFootageList(FLAGS.src_dir, analyzer = analyzer, dst_dir=FLAGS.dst_dir, reencoding=FLAGS.reencoding)
		footage_list.to_csv(FLAGS.list, index=False)
	else:
		print(f'Read Analyze {FLAGS.list}')
		footage_list = pd.read_csv(FLAGS.list, header=0)
		sliceFootages(footage_list, FLAGS.dst_dir)

if __name__ == '__main__':
	app.run(main)
