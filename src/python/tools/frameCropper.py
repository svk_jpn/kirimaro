
'''frameCropper.py
動画の指定フレームの指定領域を切り抜いて保存する。
'''
# 標準パッケージ
import os
# 外部パッケージ
from absl import app, flags
import cv2
import numpy as np
import pandas as pd
# ユーザーパッケージ

FLAGS = flags.FLAGS
flags.DEFINE_string('src_table', 'crop_table.csv', 'クロップテーブル')
flags.DEFINE_string('dst_dir', 'params/AVG2', 'クロップ画像の出力ディレクトリ')
flags.DEFINE_integer('width_s', 960, '標準化幅')
flags.DEFINE_integer('height_s', 540, '標準化高さ')

def crop_frame(cap, frame_id, roi):
	'''動画から指定フレームの指定範囲を切り抜く(0始まり)
	params:
		cap : VideoCapture インスタンス
		frame_id : 先頭を0とした場合のフレーム番号
		roi : 切り取り矩形範囲(x, y, w, h)
	'''
	frame_id = frame_id if frame_id >= 0 else 0
	cap.set(cv2.CAP_PROP_POS_FRAMES, frame_id)
	ret, frame = cap.read() # 最初のフレームを読み込む
	frame = cv2.resize(frame, [FLAGS.width_s, FLAGS.height_s])
	if ret:
		start_x = roi[0]
		start_y = roi[1]
		end_x = start_x + roi[2]
		end_y = start_y + roi[3]
		return frame[start_y:end_y, start_x:end_x]
	else:
		return None


def main(args):
	df = pd.read_csv(FLAGS.src_table)
	os.makedirs(FLAGS.dst_dir, exist_ok=True)
	for _, record in df.iterrows():
		# 動画読み込み
		cap = cv2.VideoCapture(record.src_path)
		# 切り取り情報
		frame_id = record.frame_id
		roi = [
			record.x,
			record.y,
			record.w,
			record.h
		]
		dst_path = os.path.join(FLAGS.dst_dir, record.output_name)
		img = crop_frame(cap, frame_id, roi)
		if img is not None:
			cv2.imwrite(dst_path, img)
		cap.release()

if __name__ == '__main__':
	app.run(main)
