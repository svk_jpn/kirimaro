'''Steam Capcom Aracade Stadium 版Cyberbotsゲーム開始画面の解析
'''
# 標準パッケージ
# 外部パッケージ
import numpy as np
import cv2
# ユーザーパッケージ
import tools.imgutil as iu

class CYBAnalyzer:
	'''ゲーム開始画面の分析モジュール
	ゲーム開始画面で1Pのキャラクター名がフェードインしきった箇所の解析用
	'''
	analyze_size = [960, 540] # 分析時標準サイズ
	# 各種ROI
	roi_cnames = [
		[136,78,272,40], # VS画面1P機体名
		[554,78,272,40], # VS画面2P機体名
	]
	roi_vs = [354,282,240,128] # VS表示のROI
	roi_phase1 = [560,200,208,72] # PHASE1表示のROI
	roi_destroyed = [312,284,336,80] # DESTORYED表示のROI
	roi_stalemate = [394,240,168,60] # STALEMATE表示のROI
	roi_continue = [432,96,112,56] # CONTINUE表示のROI

	# 輝度に関する閾値
	lth_continue = 128
	# 色に関する閾値
	bgr_ylogo= [10,255,251]
	cth_ylogo= 60
	
	# 特徴量に対する閾値
	fth_vs = 0.95
	fth_phase1 = 0.93
	fth_destroyed = 0.90
	fth_stalemate = 0.90
	fth_continue = 0.90

	# 動画切り取りオフセット
	offset_phase1 = -30
	offset_destoryed = 50
	offset_continue = -60

	def __init__(self, skip_analyze = 6):
		'''
		params:
			skip_analyze : 何フレームおきに解析を行うか。
		'''
		self.skip_analyze = skip_analyze # 何フレームおきに解析するか
		# 'VS'表示のテンプレート準備
		img = iu.imread('params/CYB/VS.bmp', cv2.IMREAD_COLOR)
		self.tmpl_vs = iu.bynarize_bgr(img , self.bgr_ylogo, self.cth_ylogo)
		# 'PHASE1'表示のテンプレート準備
		img = iu.imread('params/CYB/PHAS_E1.bmp', cv2.IMREAD_COLOR)
		self.tmpl_phase1 = iu.bynarize_bgr(img , self.bgr_ylogo, self.cth_ylogo)
		# 'DESTROYED'表示のテンプレート準備
		img = iu.imread('params/CYB/DE_STROY_ED.bmp', cv2.IMREAD_COLOR)
		self.tmpl_destroyed = iu.bynarize_bgr(img , self.bgr_ylogo, self.cth_ylogo)
		# 'STALEMATE'表示のテンプレート準備
		img = iu.imread('params/CYB/STA_LEM_ATE.bmp', cv2.IMREAD_COLOR)
		self.tmpl_stalemate = iu.bynarize_bgr(img , self.bgr_ylogo, self.cth_ylogo)
		# 'CONTINUE'表示のテンプレート準備
		img = iu.imread('params/CYB/CON_TIN_UE.bmp', cv2.IMREAD_GRAYSCALE)
		self.tmpl_continue = img > self.lth_continue
		# キーフレーム探索タスクリスト
		self.search_tasks = [
			self.search_match_vs,
			self.search_phase1,
			self.search_destroyed,
			self.search_stalemate,
			self.search_continue,
		]

	'''
	各種フレームの判定用メソッド
	'''
	def check_match_vs(self, img:np.ndarray):
		'''マッチ画面のVS表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_vs)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_ylogo, self.cth_ylogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_vs)
		return  score

	def check_phase1(self, img:np.ndarray):
		'''PHASE1の表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_phase1)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_ylogo, self.cth_ylogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_phase1)
		return  score

	def check_destroyed(self, img:np.ndarray):
		'''DESTROYEDの表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_destroyed)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_ylogo, self.cth_ylogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_destroyed)
		return  score

	def check_stalemate(self, img:np.ndarray):
		'''STALEMATEの表示が出ているか
		'''
		img_roi = iu.crop_roi(img, self.roi_stalemate)
		crop_bin = iu.bynarize_bgr(img_roi , self.bgr_ylogo, self.cth_ylogo)
		score = iu.match_bin_map(crop_bin, self.tmpl_stalemate)
		return  score > self.fth_stalemate

	def check_continue(self, img:np.ndarray):
		''' CONTINUEの表示が出ているか
		'''
		img = cv2.cvtColor(iu.crop_roi(img, self.roi_continue), cv2.COLOR_BGR2GRAY)
		score = iu.match_bin_map(img > self.lth_continue, self.tmpl_continue)
		return  score > self.fth_continue

	'''
	キーフレーム探索タスク
	'''
	def search_match_vs(self, fid:int):
		'''VS画面表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_match_vs(self.frame) > self.fth_vs:
			if self.f_vs == False:
				self.f_vs = True
				self.fid_info = fid
				skip_frames = 60
		else:
			self.f_vs = False
		return skip_frames

	def search_phase1(self, fid:int):
		'''PHASE1表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_phase1(self.frame) > self.fth_phase1:
			if self.f_phase1 == False:
				self.fid_start = fid
				self.fid_end = None
				self.is_dstroyed = False # DESTROYED勝ちをしたか
				self.f_phase1 = True
				skip_frames = 70
		else:
			self.f_phase1 = False
		return skip_frames

	def search_destroyed(self, fid:int):
		'''DESTROYED表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_destroyed(self.frame) > self.fth_destroyed:
			if self.f_destroyed == False:
				if self.fid_start is not None:
					self.is_dstroyed = True # DESTROYED勝ちをしたか
					self.isFindFootage = True # 切り出し区間検出
				else:
					# 区間情報のリセット
					self.fid_info = -1
					self.fid_start = None
				self.f_destroyed = True
				skip_frames = 65
		else:
			self.f_destroyed = False
		return skip_frames

	def search_stalemate(self, fid:int):
		'''STALEMATE表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_stalemate(self.frame) > self.fth_stalemate:
			if self.f_stalemate == False:
				self.fid_stalemate = fid
				self.f_stalemate = True
				skip_frames = 110
		else:
			self.f_stalemate = False
		return skip_frames

	def search_continue(self, fid:int):
		'''STALEMATE表示のフレーム探索
		'''
		skip_frames = 0
		if self.check_continue(self.frame) > self.fth_continue:
			if self.f_continue == False:
				if self.fid_start is not None and self.is_dstroyed == False:
					self.isFindFootage = True # 切り出し区間検出
				else:
					# 区間情報のリセット
					self.fid_info = -1
					self.fid_start = None
					skip_frames = 65
				self.f_continue = True
				skip_frames = 30
		else:
			self.f_continue = False
		return skip_frames

	'''解析シーケンス
	'''
	def analyze(self, cap, output_tail = False):
		'''動画の解析
		params : 
			cap : VideoCaptureインスタンス
			output_tail : 動画の最後にResult画面の無い対戦動画があった場合出力するか
		return(yield) :
			[fid_info, fid_start, fid_end]
			fid_info : マッチ情報画面のフレーム番号
			fid_start : 切り出し開始のフレーム番号
			fid_end : 切り出し終了のフレーム番号+1
		'''
		# ゲーム固有の解析フラグの初期化
		self.f_vs = False # VS画面が表示されているか
		self.f_phase1 = False # PHASE1が表示されているか
		self.f_destroyed = False # DESTROYEDが表示されているか
		self.f_stalemate = False # STALEMATEが表示されているか
		self.f_continue = False # CONTINUEが表示されているか
		self.fid_info = -1	# マッチ情報取得の動画フレーム
		self.fid_start = 0	# バトル開始のフレーム番号

		# 共通の初期化処理
		cap.set(cv2.CAP_PROP_POS_FRAMES, 0) # 読み出しフレーム位置リセット
		fps = cap.get(cv2.CAP_PROP_FPS) # 動画のFPS
		fid = 0
		ret, self.frame = cap.read() # 最初のフレームを読み込む
		self.isFindFootage = False #　動画区間を発見したか

		# 解析処理
		while(ret):
			# 前処理
			self.frame = cv2.resize(self.frame, self.analyze_size)
			# キーフレーム探索
			for task in self.search_tasks:
				skip_frames =  task(fid)
				if skip_frames > 0:
					break # キーフレーム検出
			else:
				skip_frames = self.skip_analyze # キーフレームが見つからなかった
			# 切り出し区間情報伝達
			if self.isFindFootage: # 切り出し区間の発見
				# フレーム情報の出力
				offset_end = self.offset_destoryed if self.f_destroyed else self.offset_continue
				yield { 
					'fps' : fps,
					'fid_info' : self.fid_info, 
					'fid_start' : self.fid_start + self.offset_phase1, 
					'fid_end' : fid + offset_end + 1 }
				# 区間情報のリセット
				self.isFindFootage = False
				self.fid_info = -1
				self.fid_start = None
			# 次のフレームへ
			fid, ret, self.frame = iu.read_frames(cap, fid, skip_frames)

		# 最後の動画が尻切れだった場合に出力する
		if output_tail and self.fid_start is not None:
			yield { 
				'fps' : fps,
				'fid_info' : self.fid_info, 
				'fid_start' : self.fid_start + self.offset_phase1, 
				'fid_end' : fid }